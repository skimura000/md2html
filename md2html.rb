#!/usr/bin/env ruby
require 'rubygems'
require 'kramdown'
require 'uri'

HEADER = <<-EOS
<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Document</title>
  <link rel="stylesheet" type="text/css" href="github.css">
  <link rel="stylesheet" type="text/css" href="toc.css">
  <link rel="stylesheet" href="highlight/styles/default.css">
  <script src="highlight/highlight.pack.js"></script>
  <script>hljs.initHighlightingOnLoad();</script>
</head>
<body>
EOS

FOOTER = <<-EOS
</body>
</html>
EOS

class CustomConverter < Kramdown::Converter::Html
  def convert_header(el, indent)
    el.attr[:id] = URI::encode(el.options[:raw_text])
    super(el, indent)
  end

  def convert_a(el, indent)
    if (/^#/ === el.attr["href"])
      el.attr["href"] = '#' + URI::encode($')
    end
    super(el, indent)
  end
end

def collect_headers(node, headers)
  if (node.type == :header)
    headers.push([node.options[:level], node.options[:raw_text]])
  end
  node.children.each do |child|
    collect_headers(child, headers)
  end
end

def gen_toc(node)
  ret = ''

  headers = []
  collect_headers(node, headers)

  idx = 0
  level = 0
  until (headers[idx].nil?)
    case
    when headers[idx][0] == level
      text = headers[idx][1]
      id = URI::encode(text)
      ret << "  " * headers[idx][0]
      ret << "<li><a href=\"##{id}\">#{text}</a></li>"
      ret << "\n"
      idx += 1
    when headers[idx][0] > level
      ret << "  " * headers[idx][0]
      ret << '<ul>'
      ret << "\n"
      level += 1
    when headers[idx][0] < level
      ret << "  " * headers[idx][0]
      ret << '</ul>'
      ret << "\n"
      level -= 1
    end
  end
  while (level > 0)
    ret << "  " * level
    ret << '</ul>'
    ret << "\n"
    level -= 1
  end

  return ret
end

doc =  Kramdown::Document.new(STDIN.read)

STDOUT << HEADER
STDOUT << '<div class="whole">'
STDOUT << '<div class="toc">'
STDOUT << gen_toc(doc.root)
STDOUT << '</div>'
STDOUT << '<div class="contents">'
STDOUT << CustomConverter.convert(doc.root, auto_ids: true)[0]
STDOUT << '</div>'
STDOUT << '</div>'
STDOUT << FOOTER
