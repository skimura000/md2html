# md2html

## Setup

### Ruby

T.B.D

### html-inline

~~~shell-session
$ cd md2html
$ npm install html-inline
~~~

### github.css

~~~shell-session
$ cd md2html
$ wget https://gist.githubusercontent.com/andyferra/2554919/raw/10ce87fe71b23216e3075d5648b8b9e56f7758e1/github.css
~~~

### highlight.js

1. Access https://highlightjs.org/download/ by web browser.
2. Check/Uncheck your needed languages.
3. Push 'Download' button.
4. Make 'highlight' directory on the top of md2html directory, and unzip downloaded zip file to the directory.

~~~shell-session
$ cd md2html
$ mkdir highlight
$ cd highlight
$ unzip DOWNLOAD/highlight.zip
~~~

### Kramdown

~~~shell-session
$ gem install kramdown
~~~

### Others

~~~shell-session
$ cd md2html
$ chmod +x md2html
~~~

## Usage

~~~
md2html [input-markdown] > output-html
~~~

`input-markdown` is option. When it is not specified, standard input is used.
